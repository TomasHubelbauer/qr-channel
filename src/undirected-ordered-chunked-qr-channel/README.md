# 

This should work the same way as `undirected-ordered-unchunked-qr-channel`, but `message` should be cut up to chunks.

# Planning

- Should the message be passed in as a single string? Or should there be a callback for providing next chunk?
  - Single string is inefficient when beaming files, but asking for chunks is not really chunking for the user.
  - Probably some sort of an iterator input would be the best. That way string can be used as well as a generator.
- Internally, we should probably just change the meaning of `number` to not be message number, but chunk number.
  - Everything else should be able to stay the same and work the same way.
- Through it remains to be seen (and this may be a design flaw in the other impls too) what happens when another `display` is called while beaming.
