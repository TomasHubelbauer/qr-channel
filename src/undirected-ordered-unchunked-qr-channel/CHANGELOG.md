# Changelog

## `0.8.0` 2018-04-01

Update dependency on `qr-channel` and add it as a peer dependency.

## `0.7.0` 2018-03-31

Align bundling with `qr-channel`.

## `0.6.0` 2018-03-31

Update to depend on `qr-channel` through NPM and not file system or TypeScript `import`.

## `0.5.0` 2018-03-31

Another try with WebPack.

## `0.4.0` 2018-03-31

Try again with SystemJS.

## `0.3.0` 2018-03-31

Try another test where I am compiling to a module with TSC only.

## `0.2.0` 2018-03-31

Another test release for building, this time by WebPack.

## `0.1.0` 2018-03-31

Test release testing Parcel bundling of NodeJS library packages.

## Legacy

This is a change log of the discontinued `qr-channel` package, which was split into multiple packages.

### `2.0.0` 2018-02-24

Make showing more data explicit by requiring the user to call `advance` themselves.

### `1.0.0` 2018-02-24

First version.
