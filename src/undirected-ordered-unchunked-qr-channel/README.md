# `UndirectedOrderedUnchunkedChannel`

> The peers start by displaying a "broadcast" message with no read receipt and once they meet each other, exchange messages and read receipts.
> The peers always expect the one counterparty they met first is the only other peer in the line of sight and error on meeting new peers.

The channel is *undirected*, so the read receipts do not have recipient names on them, a sole counterpart is the expected recipient.

The channel is *ordered*, so the sequential order of the received messages is verified and enforced, erroring on irregularities.

The channel is *unchunked*, so the entirety of the passed-in message is displayed in the QR code alongside the protocol metadata.

## Building

`parcel build index.ts --target=node`

## Contributing

### Protocol Metadata Structure

```js
`${name}|${number}|${counterpartyNumber}|${message}`
```

### Flow Example

- A shows `A|1||Hello from A` and scans
- B shows `B|1||Hi from B` and scans
- B sees `A|1||Hello from A` and processes it
- B knows A haven't seen any of it's messages yet because it lacks `counterpartyNumber`
- B shows `B|1|1|Hello from B` letting A know it saw its message (by adding read receipt)
- If B sees A's missing `counterpartyNumber` it knows it already shows the read receipt
- A sees `B|1|1|Hello from B` and processes it
- A knows B has seen `A|1||Hello from A` and so switches to `A|2|1|Next from A` with RR
- If A sees B's `counterpartyNumber`=`1` again it ignores it as it already shows the RR
- And so on…

## Testing

With one device:

- Open two tabs side by side:
  - `index.html#A`
  - `index.html#B`
- Alternate the webcam between the QR codes in the two tabs (use an external webcam and hold it or sit the device in front of a mirror)
- Observe as messages are exchanged and acknowledged

With two devices:

- Position the two devices against one another
- Tweak their tilt angle so that they see each other's QR codes
- Observe as messages are exchanged and acknowledged

## Publishing

- Bump version in `package.json`
- Document changes in `CHANGELOG.md`
- Update contents of `README.md`
- Verify `npm whoami` or `npm login` if needed
- `npm publish`
