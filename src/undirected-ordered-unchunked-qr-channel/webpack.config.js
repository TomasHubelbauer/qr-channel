const path = require('path');

module.exports = {
  entry: './index.ts',
  devtool: 'source-map',
  module: { rules: [ { test: /\.ts$/, use: 'ts-loader', exclude: /node_modules/ } ] },
  resolve: { extensions: [ '.ts' ] },
  output: {
    filename: '[name].js',
    path: path.join(__dirname, './dist'),
    libraryTarget: 'umd'
  },
  mode: 'development'
};
