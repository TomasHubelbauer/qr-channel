import QrChannel from 'qr-channel';
import Message from './message';

export default class Channel {
  private readonly channel: QrChannel;
  private readonly name: string;
  private number: number | null = null;
  private displayDefferedResolve: (() => void) | null = null;
  private counterpartyName: string | null = null;
  private counterpartyNumber: number | null = null;
  private readonly onMessage: (name: string, message: string) => void;

  constructor(channel: QrChannel, name: string, onMessage: (name: string, message: string) => void) {
    this.channel = channel;
    this.name = name;
    this.onMessage = onMessage;
    this.scan(); // TODO: Auto-monitor or leave up to the user?
  }

  /**
   * Display a message for the counterparty to scan and resolve when a read receipt for it comes back.
   */
  public display(message: string) {
    // Increase the number of the message being displayed.
    if (this.number === null) {
      this.number = 1;
    } else {
      this.number++;
    }

    const counterpartyMessageNumber = this.counterpartyNumber === null ? '' : this.counterpartyNumber;

    this.channel.display(`${this.name}|${this.number}|${counterpartyMessageNumber}|${message}`);
    return new Promise(resolve => {
      this.displayDefferedResolve = resolve;
    });
  }

  // TODO: Consider `monitor` and `unmonitor` and `unscan` on the decorated channel class which would do `yield break`.

  private async scan() {
    for await (const code of this.channel.scan()) {
      const message = new Message(code);

      // Meet a new counterparty or ensure we are communicating with the counterparty we already met.
      if (this.counterpartyName === null && this.counterpartyNumber === null) {
        // Freak out when having just met a peer but their message is not their first message.
        if (message.number !== 1) {
          throw new Error(`Just met ${message.name} but with message #${message.number} not 1.`);
        }

        this.counterpartyName = message.name;
        this.counterpartyNumber = 1;
        this.onMessage(this.counterpartyName!, message.message);
      } else {
        if (message.name !== this.counterpartyName!) {
          // Ignore messages from peer other than then one we met first.
          continue;
        }

        if (message.number < this.counterpartyNumber!) {
          // Freak out because the counterparty decremented the message number instead of incrementing (or name clash impostor).
          throw new Error(`${this.counterpartyName!} displayed previous message to one we already saw (or is name clashing impostor)!`);
        } else if (message.number > this.counterpartyNumber!) {
          if (message.number === this.counterpartyNumber! + 1) {
            // Process the new message!
            this.counterpartyNumber = message.number;
            // TODO: Update own message to reflect the new read receipt.
            this.onMessage(this.counterpartyName!, message.message);
          } else {
            // Freak out because the counterparty displayed next message without out read receipt!
            throw new Error(`${this.counterpartyName!} displayed next message without our read receipt!`);
          }
        } else {
          // Ignore the same message we already saw and for which we show the read receipt.
          continue;
        }
      }

      // Process read receipt logic now that we know the message is valid and for us (may still be the initial message though).
      if (message.counterpartyNumber !== null) {
        if (message.counterpartyNumber === this.number) {
          if (this.displayDefferedResolve !== null) {
            this.displayDefferedResolve();
          } else {
            // Freak out because we are displaying a message but do not have deffered resolve for it.
            throw new Error('Displaying a message but do not have deffered resolve for it.');
          }
        } else {
          // TODO: Figure out when this happens and if it is always an error or sometimes legit (scan between logic change).
        }
      } else {
        // Ignore the initial "broadcast" message sent by the counterparty looking for us.
        // TODO: May this be in the `else` branch above? Will this always be `null`? I think so because outside of the `else` we only meet.
      }
    }
  }
}
