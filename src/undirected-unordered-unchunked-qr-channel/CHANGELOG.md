# Changelog

## `initial`

- [ ] Finalize, rename and release

## Legacy:

This is a change log of the discontinued `qr-channel` package, which was split into multiple packages.

### `2.0.0` 2018-02-24

Make showing more data explicit by requiring the user to call `advance` themselves.

### `1.0.0` 2018-02-24

First version.
