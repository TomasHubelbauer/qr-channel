# `UndirectedUnorderedUnchunkedChannel`

- [ ] Document

This channel is based on the `UndirectedOrderedUnchunkedChannel`, but doesn't enforce message sequence.

## Building

Probably `parcel build index.ts --target=node`?

## Contributing

### Protocol Metadata Structure

- [ ] Document

### Flow Example

- [ ] Document

## Testing

With one device:

- Open two tabs side by side:
  - `index.html#A`
  - `index.html#B`
- Alternate the webcam between the QR codes in the two tabs (use an external webcam and hold it or sit the device in front of a mirror)
- Observe as messages are exchanged and acknowledged

With two devices:

- Position the two devices against one another
- Tweak their tilt angle so that they see each other's QR codes
- Observe as messages are exchanged and acknowledged

## Publishing

- `npm whoami`
- `npm login`
- Bump version and update changelog
- `npm publish`
