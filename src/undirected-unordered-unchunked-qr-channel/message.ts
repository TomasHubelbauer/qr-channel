export default class Message {
  public readonly name: string;
  public readonly number: number;
  public readonly counterpartyNumber: number | null;
  public readonly message: string;

  constructor(message: string) {
    this.name = 'TODO';
    this.number = 1;
    this.counterpartyNumber = null;
    this.message = message; // TODO: Trim.
  }
}
