import * as QrChannel from '../Channel';
import Message from './Message';

export default class Channel {
  private readonly channel: QrChannel.default;
  private readonly name: string;
  private number: number | null = null;
  private counterpartyName: string | null = null;
  private displayDefferedResolves: { [number: number]: (() => void) | undefined } = {};
  private counterpartyNumber: number | null = null;
  private readonly onMessage: (name: string, message: string) => void;

  constructor(channel: QrChannel.default, name: string, onMessage: (name: string, message: string) => void) {
    this.channel = channel;
    this.name = name;
    this.onMessage = onMessage;
    this.scan(); // TODO: Auto-monitor or leave up to the user?
  }

  /**
   * Display a message for the counterparty to scan and resolve when a read receipt for it comes back.
   * @param async Whether or not to return a promise that will be resolved when a read receipt for the message arrives.
   */
  public display(message: string, async: boolean) {
    // Increase the number of the message being displayed.
    if (this.number === null) {
      this.number = 1;
    } else {
      this.number++;
    }

    const counterpartyMessageNumber = this.counterpartyNumber === null ? '' : this.counterpartyNumber;

    this.channel.display(`${this.name}|${this.number}|${counterpartyMessageNumber}|${message}`);
    if (async) {
      return new Promise(resolve => {
        this.displayDefferedResolves[this.number!] = resolve;
      });
    }
  }

  private async scan() {
    for await (const code of this.channel.scan()) {
      const message = new Message(code);

      // Meet a new counterparty or ensure we are communicating with the counterparty we already met.
      if (this.counterpartyName === null && this.counterpartyNumber === null) {
        this.counterpartyName = message.name;
        this.counterpartyNumber = message.number;
        this.onMessage(this.counterpartyName!, message.message);
      } else {
        if (message.name !== this.counterpartyName!) {
          // Ignore messages from peer other than then one we met first.
          continue;
        }

        if (message.number !== this.counterpartyNumber!) {
          // Process the new message!
          this.counterpartyNumber = message.number;
          // TODO: Update own message to reflect the new read receipt.
          this.onMessage(this.counterpartyName!, message.message);
        } else {
          // Ignore the same message we already saw and for which we show the read receipt.
          continue;
        }
      }

      // Process read receipt logic now that we know the message is valid and for us (may still be the initial message though).
      if (message.counterpartyNumber !== null) {
        const deferredResolve = this.displayDefferedResolves[message.counterpartyNumber];
        if (deferredResolve !== undefined) {
          deferredResolve();
        } else {
          // Freak out because we are displaying a message but do not have deffered resolve for it.
          throw new Error('Displaying a message but do not have deffered resolve for it.');
        }
      } else {
        // Ignore the initial "broadcast" message sent by the counterparty looking for us.
        // TODO: May this be in the `else` branch above? Will this always be `null`? I think so because outside of the `else` we only meet.
      }
    }
  }
}
