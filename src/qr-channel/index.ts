import * as jsQR_ from 'jsqr';
import * as qrcode_ from 'qrcode-generator';

const jsQR: typeof jsQR_.default = jsQR_ as any;
const qrcode: typeof qrcode_ = qrcode_;

export type TypeNumber =
  | 0 // Automatic type number
  | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10
  | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20
  | 21 | 22 | 23 | 24 | 25 | 26 | 27 | 28 | 29 | 30
  | 31 | 32 | 33 | 34 | 35 | 36 | 37 | 38 | 39 | 40
  ;

export type ErrorCorrectionLevel = 'L' | 'M' | 'Q' | 'H';

export type Mode = 'Numeric' | 'Alphanumeric' | 'Byte' /* Default */;

export default class Channel {
  private readonly codeCanvas: HTMLCanvasElement;
  private readonly viewfinderVideo: HTMLVideoElement;
  private readonly snapshotCanvas: HTMLCanvasElement;
  private readonly typeNumber: TypeNumber;
  private readonly errorCorrectionLevel: ErrorCorrectionLevel;
  private readonly mode: Mode;

  constructor(
    codeCanvas: HTMLCanvasElement,
    viewfinderVideo: HTMLVideoElement,
    snapshotCanvas: HTMLCanvasElement,
    typeNumber: TypeNumber,
    errorCorrectionLevel: ErrorCorrectionLevel,
    mode: Mode) {
    this.codeCanvas = codeCanvas;
    this.viewfinderVideo = viewfinderVideo;
    this.snapshotCanvas = snapshotCanvas;
    this.typeNumber = typeNumber;
    this.errorCorrectionLevel = errorCorrectionLevel;
    this.mode = mode;
  }

  /**
   * Displays a message as a QR code in the code canvas.
   * @param message The message to display as a QR code.
   * @param typeNumber The QR code type number, 0 for automatic or 1-40 for manual.
   * @param errorCorrectionLevel The QR code correction level. `L`, `M`, `Q` or `H`.
   * @param mode The QR code mode, defaults to the `byte` mode.
   * @returns void
   */
  public display(message: string) {
    // See notes for maximal capacity
    const code = qrcode(this.typeNumber, this.errorCorrectionLevel);
    code.addData(message.toString(), this.mode);
    code.make();

    // Make the context fit the current code canvas dimensions
    let { width, height } = this.codeCanvas.getBoundingClientRect();
    this.codeCanvas.width = (width = Math.floor(width));
    this.codeCanvas.height = (height = Math.floor(height));

    const context = this.codeCanvas.getContext('2d');
    if (context === null) {
      throw new Error('2D context is not supported.');
    }

    const count = code.getModuleCount();
    const length = Math.min(height, width);
    const floorSize = Math.floor(length / count);
    const ceilSize = Math.ceil(length / count);

    // Center the QR code within the code canvas
    const x = (width - length) / 2;
    const y = (height - length) / 2;

    for (let row = 0; row < count; row++) {
      for (let col = 0; col < count; col++) {
        context.fillStyle = code.isDark(row, col) ? 'black' : 'white';
        context.fillRect(x + row * floorSize, y + col * floorSize, ceilSize, ceilSize);
      }
    }
  }

  /**
   * Scans the web camera stream for QR codes and yields them as they are recognized.
   * @returns AsyncIterableIterator<string>, use by `for await (const code of channel.scan()) { … }`
   */
  public async *scan() {
    // Setup viewfinder view and snapshot canvas on the first call.
    if (this.viewfinderVideo.srcObject === null) {
      this.viewfinderVideo.srcObject = await window.navigator.mediaDevices.getUserMedia({ audio: false, video: true });
      await this.viewfinderVideo.play();

      // Obtain the viewfinder video dimensions.
      const { videoHeight, videoWidth } = this.viewfinderVideo;
      if (videoHeight === 0 && videoWidth === 0) {
        throw new Error('Playing, but video dimensions are still zero.');
      }
    
      // Resize the snapshot canvas to match the viewfinder video dimensions.
      this.snapshotCanvas.height = videoHeight;
      this.snapshotCanvas.width = videoWidth
    }

    do {
      const message = this.snap();
      if (message !== null) {
        yield message;
      }
    } while (await this.wait());
  }

  // TODO: Consider `unscan` which would `yield break` and `reject` the `wait` promise to enable `unmonitor` in derived classes

  private snap() {
    const context = this.snapshotCanvas.getContext('2d');
    if (context === null) {
      throw new Error('2D context is not supported.');
    }

    context.drawImage(this.viewfinderVideo, 0, 0, this.viewfinderVideo.videoWidth, this.viewfinderVideo.videoHeight);

    const timestamp = window.performance.now();
    const { data, width, height } = context.getImageData(0, 0, this.snapshotCanvas.width, this.snapshotCanvas.height);
    const code = jsQR(data, width, height);
    if (code !== null) {
      context.moveTo(code.location.topLeftCorner.x, code.location.topLeftCorner.y);
      context.lineTo(code.location.topRightCorner.x, code.location.topRightCorner.y);
      context.lineTo(code.location.bottomRightCorner.x, code.location.bottomRightCorner.y);
      context.lineTo(code.location.bottomLeftCorner.x, code.location.bottomLeftCorner.y);
      context.lineTo(code.location.topLeftCorner.x, code.location.topLeftCorner.y);
      context.stroke();
      return code.data;
    }

    return null;
  }

  private wait() {
    // TODO: Store this promise as a class field and reject it when `unscan` is called if we end up having that.
    return new Promise((resolve, reject) => {
      if (window.requestIdleCallback) {
        window.requestIdleCallback(resolve); // non-zero number so `do`-`while` continues
      } else {
        window.setTimeout(resolve, 0, true); // `true` so that `do`-`while` continues
      }
    });
  }
}

declare global {
  interface Window {
    requestIdleCallback(callback: (timestamp: number) => void): number;
    cancelIdleCallback(handle: number): void;
  }
}
