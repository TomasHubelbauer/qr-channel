# QR Channel

```js
import Channel from 'qr-channel'

const qrChannel = new Channel(
  document.querySelector('#codeCanvas'),
  document.querySelector('#viewfinderVideo'),
  document.querySelector('#snapshotCanvas'),
  0,
  'H',
  'Byte'
);

qrChannel.display('Message');

void async function monitor() {
  for await (const message of qrChannel.scan()) {
    console.log(message);
  }
}
```

## Installing

`yarn add qr-channel`

## Building

`webpack-cli`

## Testing

See the demo in `demo/protocol/qr-channel`.

## Publishing

- Bump version in `package.json`
- Document changes in `CHANGELOG.md`
- Update contents of `README.md`
- Verify `npm whoami` or `npm login` if needed
- `npm publish`

## Contributing
