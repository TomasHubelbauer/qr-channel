# Changelog

## `5.0.1` 2018-04-01

Publish the right artifacts…

## `5.0.0` 2018-04-01

Make the class non-`abstract` so it can be used directly without a protocol.

## `4.0.7` 2018-03-31

Hopefully settle the modules thing and remove `library`.

## `4.0.6` 2018-03-31

Try more export tweaks.

## `4.0.5` 2018-03-31

Try with CommonJS2 instead.

## `4.0.4` 2018-03-31

Use `libraryExport` to get rid of the `default` indirection.

## `4.0.3` 2018-03-31

Rename `library` to `QrChannel`.

## `4.0.2` 2018-03-31

Set `libraryTarget` to `umd`.

## `4.0.1` 2018-03-31

Set `libraryTarget` to `commonjs`.

## `4.0.0` 2018-03-31

Generate a version with typings.

## `3.0.0` 2018-03-31

Release the first version after the split into multiple packages. The core API now: `display`, `scan`.

## Legacy

This is a change log of the discontinued `qr-channel` package, which was split into multiple packages.

### `2.0.0` 2018-02-24

Make showing more data explicit by requiring the user to call `advance` themselves.

### `1.0.0` 2018-02-24

First version.
