import EventTarget from './EventTarget';

type ReceivedMessage = { number: number; chunkCount: number; chunks: string[]; };

type ChunkSentEventHandler = (event: ChunkSentEvent) => void;
class ChunkSentEvent extends Event {
  constructor() {
    super('chunkSent');
  }
}

type ChunkReceivedEventHandler = (event: ChunkReceivedEvent) => void;
class ChunkReceivedEvent extends Event {
  constructor() {
    super('chunkReceived');
  }
}

type MessageEventHandler = (event: MessageEvent) => void;
class MessageEvent extends Event {
  constructor() {
    super('message');
  }
}

// TODO: Make this an event emitter.
export default class Peer extends EventTarget {
  public readonly name: string;
  public readonly receivedMessages: ReceivedMessage[] = [];
  
  constructor(name: string) {
    super();
    this.name = name;
  }

  public addEventListener(type: 'chunkSent', listener: ChunkSentEventHandler | EventListenerObject | undefined): void;
  public addEventListener(type: 'chunkReceived', listener: ChunkReceivedEventHandler | EventListenerObject | undefined): void;
  public addEventListener(type: 'message', listener: MessageEventHandler | EventListenerObject | undefined): void;
  public addEventListener(type: string, listener: EventListener | EventListenerObject) { super.addEventListener(type, listener); }

  public feed(messageNumber: number, messageChunkNumber: number, messageChunkCount: number, payload: string) {
    let message = this.receivedMessages.find(m => m.number === messageNumber);
    if (!message) {
      message = { number: messageNumber, chunkCount: messageChunkCount, chunks: [] };
      this.receivedMessages.push(message);
    } else if (message.chunkCount !== messageChunkCount) {
      throw new Error('Inconsistent chunk count provided across received messages for a given message.');
    }

    message.chunks[messageChunkNumber - 1] = payload;
    if (message.chunks.length === message.chunkCount) {
      // TODO: Emit `message` event.
      // TODO: Resolve the `cast` promise to be passed through the constructor here.
    }
  }
}
