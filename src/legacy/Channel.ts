import Message from './Message';
import Peer from './Peer';

type ReadReceipt = { number: number; recipientName: string; recipientMessageNumber: number; recipientMessageChunkNumber: number; recipientReadReceiptNumber: number; };

type SentMessage = { message: string; messageNumber: number; recipientName: string | null; };

// TODO: Make this an event emitter.
export default class Channel extends EventTarget {
  public static readonly SCANNING_TIMEOUT_MS = 25;
  public static readonly CYCLING_TIMEOUT_MS = 25;
  public static readonly MESSAGE_PAYLOAD_SIZE = 40;

  private messages: SentMessage[] = [];
  private messageNumber = 0;
  private messageIndex = 0;
  private messageChunkIndex = 0;

  private peers: Peer[] = [];

  private messageReadReceipts: MessageReadReceipt[] = [];
  private messageReadReceiptNumber = 0;
  private messageReadReceiptIndex = 0;

  private readReceiptReadReceipts: ReadReceiptReadReceipt[] = [];
  private readReceiptReadReceiptNumber = 0;
  private readReceiptReadReceiptIndex = 0;

  private scanningHandle: number | undefined;
  private cyclingHandle: number | undefined;

  constructor(name: string) {
    super();
    this.name = name;

    // Self-bind the functions as they will be used as a callbacks.
    this.scan = this.scan.bind(this);
    this.cycle = this.cycle.bind(this);
  }

  /**
   * Ceases periodically scanning the webcam steam for QR codes and unloads it.
   */
  public unmonitor() {
    if (this.scanningHandle !== undefined) {
      if (window.cancelIdleCallback) {
        window.cancelIdleCallback(this.scanningHandle);
      } else {
        window.clearTimeout(this.scanningHandle);
      }

      this.scanningHandle = undefined;
    }

    // Destroy the viewfinder video media stream from the webcam.
    this.viewfinderVideo.srcObject = null;

    // Clear the snapsghot canvas properly without using the attribute hack (slow and unsupported: https://stackoverflow.com/a/6722031/2715716).
    const context = this.snapshotCanvas.getContext('2d');
    if (context === null) {
      throw new Error('2D context is not supported.');
    }

    context.clearRect(0, 0, this.snapshotCanvas.width, this.snapshotCanvas.height);
  }

  /**
   * Processes a scanned QR message by updating peers based on its content.
   * @param message The scanned QR message.
   */
  private process(message: Message) {
    if (message.senderName === this.name) {
      // Ignore self-message (captured in reflection or something).
      return;
    }

    if (message.recipientName !== this.name) {
      // Skip messages not directed for us.
      return;
    }

    // TODO: Create read receipt for message and read receipt only if they don't already exist
    this.messageReadReceiptNumber++;
    this.messageReadReceipts.push({
      number: this.messageReadReceiptNumber,
      recipientName: message.senderName,
      recipientMessageNumber: message.senderMessageNumber,
      recipientMessageChunkNumber: message.senderMessageChunkNumber,
      recipientReadReceiptNumber: TODO // TODO: Maybe split into read receipts for message and read receipts for read receipts.
    });

    let peer = this.peers.find(p => p.name === message.senderName);
    if (peer === undefined) {
      peer = new Peer(message.senderName); // TODO: Pass the `cast` promise here.
      this.peers.push(peer);
      this.dispatchEvent(new PeerEvent(peer));
    }

    if (message.readReceipt !== null) {
      const { recipientMessageNumber, recipientMessageChunkNumber, recipientReadReceiptNumber } = message.readReceipt;
      const index = this.messageReadReceipts.findIndex(rr => rr.number === recipientReadReceiptNumber);
      
      // Ignore not-found read receipts, because we will get a few scans with the read receipt until the counterparty sees ours where we tell it we saw it.
      if (index !== -1) {
        this.messageReadReceipts.splice(index, 1);
      }

      // TODO: Emit `sentProgress`, `receivedProgress` and `message` events on Peer.
      // TODO: Resolve `cast` promise once message is fully sent and read receipt confirms that.
    }

    peer.feed(message.senderMessageNumber, message.senderMessageChunkNumber, message.senderMessageChunkCount, message.payload);
  }

  /**
   * Broadcasts a message to all peers.
   * @param message The message to broadcast.
   */
  public broadcast(message: string) {
    this.cast(null, message);
  }

  /**
   * Removes a message from the broadcast.
   * @param message The message to remove.
   */
  public unbroadcast(message: string) {
    const index = this.messages.findIndex(m => m.recipientName === null /* Broadcast */ && m.message === message);
    if (index === -1) {
      throw new Error(`There is no broadcast for the message '${message}'.`);
    }

    this.messages.splice(index, 1);
  }

  /**
   * Casts a message to a specified recipient peer.
   * @param recipientName The name of the intended recipient of the message.
   * @param message The message to cast.
   */
  private cast(recipientName: string | null /* Broadcast */, message: string) {
    this.messageNumber++;
    this.messages.push({ message, messageNumber: this.messageNumber, recipientName });

    // Start cycling unless we already are (then we will get to the new message by it).
    if (this.cyclingHandle === undefined) {
      this.cycle();
    }
  }

  /**
   * Displays the current message and read receipt and schedules advancing the message and read receipt to the next, or stops if there is nothing to advance to.
   */
  private cycle() {
    const message = this.messages[this.messageIndex];
    const chunkIndex = this.messageChunkIndex * Channel.MESSAGE_PAYLOAD_SIZE;
    const chunk = message.message.slice(chunkIndex, chunkIndex + Channel.MESSAGE_PAYLOAD_SIZE);
    const chunkCount = Math.ceil(message.message.length / Channel.MESSAGE_PAYLOAD_SIZE);
    const readReceipt = this.messageReadReceipts.length > 0 ? this.messageReadReceipts[this.messageReadReceiptIndex] : null;
    this.display(0, 'H', new Message(this.name, message.messageNumber, this.messageChunkIndex + 1, chunkCount, message.recipientName, readReceipt, chunk));

    // Move to next chunk (if any), next message (if any) or stop cycling.
    if (this.messageChunkIndex === chunkCount - 1) {
      this.messageChunkIndex = 0;
      // Move to next message (if any) or stop cycling.
      if (this.messageIndex === this.messages.length - 1) {
        this.messageIndex = 0;
        // Stop cycling and keep displaying the only chunk unless we have read receipts to keep cycling through on this one message.
        if (this.messageReadReceipts.length === 0 && (this.messages.length === 0 || (this.messages.length === 1 && this.messages[0].message.length < Channel.MESSAGE_PAYLOAD_SIZE))) {
          this.cyclingHandle = undefined;
          return;
        }
      } else {
        this.messageIndex++;
      }
    } else {
      this.messageChunkIndex++;
    }

    // Move onto the next read receipt to show or wrap around to the first again.
    if (this.messageReadReceipts.length > 1 /* Sole read receipt doesn't need advancing. */) {
      if (this.messageReadReceiptIndex === this.messageReadReceipts.length - 1) {
        this.messageReadReceiptIndex = 0;
      } else {
        this.messageReadReceiptIndex++;
      }
    }

    // Cycle to the next chunk or the next message.
    this.cyclingHandle = window.setTimeout(this.cycle, 100);
  }
}
