import ReadReceipt from './ReadReceipt';

type Values = Partial<{
  senderName: string;
  senderMessageNumber: number;
  senderMessageChunkNumber: number;
  senderMessageChunkCount: number;
  recipientName: string | null;
  readReceiptRecipientName: string;
  readReceiptRecipientMessageNumber: number;
  readReceiptRecipientMessageChunkNumber: number;
  readReceiptRecipientReadReceiptNumber: number;
  payload: string;
}>;

class IncompleteMessageError extends Error {
  public readonly values: Values;
  public readonly message: string;
  constructor(values: Values, message: string) {
    super(``);
    this.values = values;
    this.message = message;
  }
}

class HeaderParsingError extends Error {
  public readonly message: string;
  constructor(message: string) {
    super(`The message does not start with the expected header '${Message.HEADER}'.`);
    this.message = message;
  }
}

class SenderNameParsingError extends Error {
  public readonly data: string;
  public readonly message: string;
  constructor(data: string, message: string) {
    super(`Failed to parse sender name. The value '${data}' is empty.`);
    this.data = data;
    this.message = message;
  }
}

class SenderMessageNumberParsingError extends Error {
  public readonly data: string;
  public readonly message: string;
  constructor(data: string, message: string) {
    super(`Failed to parse sender message number. The value '${data}' is empty or does not represent a number.`);
    this.data = data;
    this.message = message;
  }
}

class SenderMessageChunkNumberParsingError extends Error {
  public readonly data: string;
  public readonly message: string;
  constructor(data: string, message: string) {
    super(`Failed to parse sender message chunk number. The value '${data}' is empty or does not represent a number.`);
    this.data = data;
    this.message = message;
  }
}

class SenderMessageChunkCountParsingError extends Error {
  public readonly data: string;
  public readonly message: string;
  constructor(data: string, message: string) {
    super(`Failed to parse sender message chunk count. The value '${data}' is empty or does not represent a number.`);
    this.data = data;
    this.message = message;
  }
}

class ReadReceiptRecipientMessageNumberParsingError extends Error {
  public readonly data: string;
  public readonly message: string;
  constructor(data: string, message: string) {
    super(`Failed to parse read receipt recipient message number. The value '${data}' is empty or does not represent a number.`);
    this.data = data;
    this.message = message;
  }
}

class ReadReceiptRecipientMessageChunkNumberParsingError extends Error {
  public readonly data: string;
  public readonly message: string;
  constructor(data: string, message: string) {
    super(`Failed to parse read receipt recipient message chunk number. The value '${data}' is empty or does not represent a number.`);
    this.data = data;
    this.message = message;
  }
}

class ReadReceiptRecipientReadReceiptNumberParsingError extends Error {
  public readonly data: string;
  public readonly message: string;
  constructor(data: string, message: string) {
    super(`Failed to parse read receipt recipient read receipt number. The value '${data}' is empty or does not represent a number.`);
    this.data = data;
    this.message = message;
  }
}

export default class Message {
  public readonly senderName: string;
  public readonly senderMessageNumber: number;
  public readonly senderMessageChunkNumber: number;
  public readonly senderMessageChunkCount: number;
  public readonly recipientName: string | null;
  public readonly readReceipt: ReadReceipt | null;
  public readonly payload: string;

  private static readonly SEPARATOR = ' ';
  public static readonly HEADER = 'QR';

  constructor(
    senderName: string,
    senderMessageNumber: number,
    senderMessageChunkNumber: number,
    senderMessageChunkCount: number,
    recipientName: string | null,
    readReceipt: ReadReceipt | null,
    payload: string
  ) {
    this.senderName = senderName;
    this.senderMessageNumber = senderMessageNumber;
    this.senderMessageChunkNumber = senderMessageChunkNumber;
    this.senderMessageChunkCount = senderMessageChunkCount;
    this.recipientName = recipientName;
    this.readReceipt = readReceipt;
    this.payload = payload;
  }

  public static parse(data: string): Message {
    const message = data;
    let index;
    
    // Ensure we have a correct header (`QR`).
    if (!data.startsWith(Message.HEADER)) throw new HeaderParsingError(message);
    data = data.slice(Message.HEADER.length);

    // Parse sender name:
    index = data.indexOf(Message.SEPARATOR);
    if (index === -1) throw new IncompleteMessageError({}, message);
    const senderName = data.substr(0, index);
    if (senderName === '') throw new SenderNameParsingError(senderName, message);
    data = data.slice(senderName.length + Message.SEPARATOR.length);

    // Parse sender message number:
    index = data.indexOf(Message.SEPARATOR);
    if (index === -1) throw new IncompleteMessageError({ senderName }, message);
    const senderMessageNumberData = data.substr(0, index);
    if (senderMessageNumberData === '' || !/\d+/.test(senderMessageNumberData)) throw new SenderMessageNumberParsingError(senderMessageNumberData, message);
    data = data.slice(senderMessageNumberData.length + Message.SEPARATOR.length);
    const senderMessageNumber = Number(senderMessageNumberData);

    // Parse sender message chunk number:
    index = data.indexOf(Message.SEPARATOR);
    if (index === -1) throw new IncompleteMessageError({ senderName, senderMessageNumber }, message);
    const senderMessageChunkNumberData = data.substr(0, index);
    if (senderMessageChunkNumberData === '' || !/\d+/.test(senderMessageChunkNumberData)) throw new SenderMessageChunkNumberParsingError(senderMessageChunkNumberData, message);
    data = data.slice(senderMessageChunkNumberData.length + Message.SEPARATOR.length);
    const senderMessageChunkNumber = Number(senderMessageChunkNumberData);

    // Parse sender message chunk count:
    index = data.indexOf(Message.SEPARATOR);
    if (index === -1) throw new IncompleteMessageError({ senderName, senderMessageNumber, senderMessageChunkNumber }, message);
    const senderMessageChunkCountData = data.substr(0, index);
    if (senderMessageChunkCountData === '' || !/\d+/.test(senderMessageChunkCountData)) throw new SenderMessageChunkCountParsingError(senderMessageChunkCountData, message);
    data = data.slice(senderMessageChunkCountData.length + Message.SEPARATOR.length);
    const senderMessageChunkCount = Number(senderMessageChunkCountData);

    // Parse recipient name:
    index = data.indexOf(Message.SEPARATOR);
    if (index === -1) throw new IncompleteMessageError({ senderName, senderMessageNumber, senderMessageChunkNumber, senderMessageChunkCount }, message);
    let recipientName: string | null /* Broadcast */ = data.substr(0, index);
    data = data.slice(recipientName.length + Message.SEPARATOR.length);
    if (recipientName === '') {
      // Mark the message as a broadcast message.
      recipientName = null;
    }

    // Parse read receipt recipient name:
    index = data.indexOf(Message.SEPARATOR);
    if (index === -1) throw new IncompleteMessageError({ senderName, senderMessageNumber, senderMessageChunkNumber, senderMessageChunkCount, recipientName }, message);
    let readReceiptRecipientName: string = data.substr(0, index);
    data = data.slice(readReceiptRecipientName.length + Message.SEPARATOR.length);
    
    let readReceipt: ReadReceipt | null;
    if (readReceiptRecipientName !== '') {
      // Parse read receipt recipient message number:
      index = data.indexOf(Message.SEPARATOR);
      if (index === -1) throw new IncompleteMessageError({ senderName, senderMessageNumber, senderMessageChunkNumber, readReceiptRecipientName }, message);
      const readReceiptRecipientMessageNumberData = data.substr(0, index);
      if (readReceiptRecipientMessageNumberData === '' || !/\d+/.test(readReceiptRecipientMessageNumberData)) throw new ReadReceiptRecipientMessageNumberParsingError(readReceiptRecipientMessageNumberData, message);
      data = data.slice(readReceiptRecipientMessageNumberData.length + Message.SEPARATOR.length);
      const readReceiptRecipientMessageNumber = Number(readReceiptRecipientMessageNumberData);

      // Parse read receipt recipient message chunk number:
      index = data.indexOf(Message.SEPARATOR);
      if (index === -1) throw new IncompleteMessageError({ senderName, senderMessageNumber, senderMessageChunkNumber, readReceiptRecipientName, readReceiptRecipientMessageNumber }, message);
      const readReceiptRecipientMessageChunkNumberData = data.substr(0, index);
      if (readReceiptRecipientMessageChunkNumberData === '' || !/\d+/.test(readReceiptRecipientMessageChunkNumberData)) throw new ReadReceiptRecipientMessageChunkNumberParsingError(readReceiptRecipientMessageChunkNumberData, message);
      data = data.slice(readReceiptRecipientMessageChunkNumberData.length + Message.SEPARATOR.length);
      const readReceiptRecipientMessageChunkNumber = Number(readReceiptRecipientMessageChunkNumberData);

      // Parse read receipt recipient read receipt number:
      index = data.indexOf(Message.SEPARATOR);
      if (index === -1) throw new IncompleteMessageError({ senderName, senderMessageNumber, senderMessageChunkNumber, readReceiptRecipientName, readReceiptRecipientMessageNumber, readReceiptRecipientMessageChunkNumber }, message);
      const readReceiptRecipientReadReceiptNumberData = data.substr(0, index);
      if (readReceiptRecipientReadReceiptNumberData === '' || !/\d+/.test(readReceiptRecipientReadReceiptNumberData)) throw new ReadReceiptRecipientReadReceiptNumberParsingError(readReceiptRecipientReadReceiptNumberData, message);
      data = data.slice(readReceiptRecipientReadReceiptNumberData.length + Message.SEPARATOR.length);
      const readReceiptRecipientReadReceiptNumber = Number(readReceiptRecipientReadReceiptNumberData);

      // TODO: Handle both read receipts for messages and read receipts for read receipts if we end up splitting this.

      readReceipt = { recipientName: readReceiptRecipientName, recipientMessageNumber: readReceiptRecipientMessageNumber, recipientMessageChunkNumber: readReceiptRecipientMessageChunkNumber, recipientReadReceiptNumber: readReceiptRecipientReadReceiptNumber };
    } else {
      readReceipt = null;
    }

    // Consider the payload to be the remaining data now that we've cut off all of the metadata.
    const payload = data;

    return new Message(senderName, senderMessageNumber, senderMessageChunkNumber, senderMessageChunkCount, recipientName, readReceipt, payload);
  }

  public toString() {
    const readReceipt = this.readReceipt !== null ? `${this.readReceipt.recipientName} ${this.readReceipt.recipientMessageNumber} ${this.readReceipt.recipientMessageChunkNumber} ${this.readReceipt.recipientReadReceiptNumber}` : '';
    return `${Message.HEADER} ${this.senderName} ${this.senderMessageNumber} ${this.senderMessageChunkNumber} ${this.senderMessageChunkCount} ${this.recipientName} ${readReceipt} ${this.payload}`;
  }
}
