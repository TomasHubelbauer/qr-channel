# Benchmark all protocols in various scenarios

Benchmark the protocols in various scenarios. See legacy text below.

The peer would display a conservatively small QR code as a first step, like 30 characters in binary mode.

Once another peer is spotted, next codes will be bigger and bigger, packing more and more characters.
This growing will happens as long as the counterparty is able to see and confirm our codes in a timely manner (less than a second say).
Once we lose a peer for too long, we restart from the smallest codes (or shrink linearly until reconnected?).

This way we won't need any magic constants and all devices in all peer configurations should end up selecting the largest possible code
within the constraints of the peer still seeing it.

If the connection gets legitimately interrupted, it will come to a halt with the smallest code and when
the devices see each other again, it will start growing again until idea conditions are met again.
