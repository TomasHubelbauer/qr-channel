# Develop missing channels

The various scenarios in which QR channel can be utilized cannot possibly be served by a single opinionated API.
The tradeoffs involved in exchanging messages between peers have too much of an impact to assume the role of their maker.
Instead of attempting to settle on a single set of decisions, various channel implementations are exposed, each suited for a diffent set of scenarios.

The names of the channel implementations follow these rules:

- Direction of read receipts
  - `Undirected` for when the read receipts are not directed at any peer in channels where a single counterparty only is assumed and supported
  - `Directed` for when read receipts are accompanied by the recipient name in channels which support networks of peers communicating amongst one another
- Order of the messages and tolerance to missed messages (if line of sight between the cameras and QR codes is lost in some moments)
  - `Unordered` for when the channel doesn't check whether the received messages are coming in a continuous order
  - `Ordered` for when the channel does ensure the messages are coming in a sequence and starting from the initial one
- Whether or not the channel chunks the messages for the user (displaying several QR codes per message if needed) based on a cutoff length constraint
  - `Unchunked` for when the channel displays only a single QR code per message and displays impractically huge QR codes or errors on really long messages
  - `Chunked` for when the channel does chunk the messages based on configured constraints transparently to the user who still sees the message API
- For chunked channels, the read receipt strategy name is appeneded
  - `Combined` for when the channel displays read receipts for all peers in each QR code (cutting into the available message length per QR code)
  - `Cycled` for when the channel displays a chunk several times with different read receipts for different peers trading scan time for QR code size
  - Possibly others…

The channels are decorators on top of the underlaying `Channel` implementation which displays and scans bare messages (with no protocol metadata in them).

## Missing protocols

See [../doc/read-receipts.md] for inspiration on what read receipt strategies are still missing.

- [ ] Undirected unordered/ordered chunked … (multiple variants of the read receipt strategy)
- [ ] Directed unordred/ordered chunked … (multiple variants of the read receipt strategy)

## Cast v. broadcast messages idea

There is this idea of mixing sequential and insequential messages in a single channel.
The purpose of that would be to enable quick exchange of ICE while SDP is being exchanged in WebRTC.

I don't know whether ICE candidates are specific to a WebRTC connection (so specific to different peers) or not.

If yes:

- The ICE candidate messages would still need receipts from the individual peers and would have to be cast directly at specific peers.
- It would be akin to having two parallel tracks of sequential exchange which bears no benefit compared to a single track in terms of bandwidth, probably.
  - Unless exchanging SDP was slow and ICE was fast in which case slowing down ICE by SDP would make no sense.
  - But this is probably generalizeable to a channel which cycles messages where ICE messages would cycle more times and ICE less times until read receipt.

- If no:

- The ICE candidate messages could just be rotated forever knowing that all peers will eventually see them (the idea of broadcast messages).
- This would be the implementation with `cast` for peer-specific messages and `broadcast` messages, and the chunks and read receipts would be cycled (?).
