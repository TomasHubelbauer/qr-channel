# Finalize combinatorically generated test flows

See `src/spike/index.ts` for algorithm that spits out all the possible variations of the flow for a given N of ICE candidates code draft.

- A displays an offer
- B displays an offer
- A notices B offer
- A generates an answer
- B notices A offer
- B generates an answer
- A displays an answer for B
- B displays an answer for A
- A notices the answer from B
- A ignores answer in answering state
- …
- Infinite stall? (How to resolve this? Names?)

---

- A displays an offer
- B displays an offer
- A notices B offer
- A displays an answer
- B notices A answer
- B displays a pranswer
- A notices B pranswer
- Connection established? (Without interactive ICE - either no ICE or embedded in SDP)

---

- A displays an offer
- B displays an offer
- A notices B offer
- A displays an answer
- B notices A answer
- B displays candidate 1/3
- A notices B candidate
- A displays candidate 1/3
- B notices A candidate
- B displays candidate 2/3
- A notices B candidate
- A displays candidate 2/3
- …
- Connection established?
