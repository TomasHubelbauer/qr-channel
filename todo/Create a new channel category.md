# Create a new channel category

Current naming conventions say that either the channel is unchunked in which case it's none or it is and there is a suffix for chunking category.

But we need a new channel which is unchunked, but rotates messages in with read receipts.

So I think we need these components to the name:

- `unsigned`/`signed` - assume 2 peers (and do not include any signatures/names for checking, ignore own messages by knowing what is shown)
  - But for non-unique things like peer candidates this may be a problem, maybe those need to be prefixed with whether they are offer or answer candidates
- `unordered`/`ordered` - whether or not the channel ignores incoming messages if they are not the next in the sequence
- Here I though we should distinguish channels who replace last message with a new one and that's it, but that could never work with read receipts
  - We always need to be either cycling or adding read receipts to the messages being shown
  - The question is do we show a message until confirmed seen or loop messages quickly hoping to maximize notice speed and wait for RRs asyncly
- The actual chunking strategy
