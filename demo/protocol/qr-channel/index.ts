import QrChannel from 'qr-channel';

window.addEventListener('load', async _ => {
  const codeCanvas = document.querySelector<HTMLCanvasElement>('#codeCanvas');
  const viewfinderVideo = document.querySelector<HTMLVideoElement>('#viewfinderVideo');
  const snapshotCanvas = document.querySelector<HTMLCanvasElement>('#snapshotCanvas');

  const qrChannel = new QrChannel(codeCanvas, viewfinderVideo, snapshotCanvas, 0, 'H', 'Byte');
  qrChannel.display('Hello, World!');
  for await (const code of qrChannel.scan()) {
    console.log('code');
    qrChannel.display(code);
  }
});
