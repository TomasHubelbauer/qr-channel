import QrChannel from 'qr-channel';
import ProtocolQrChannel from 'undirected-ordered-unchunked-qr-channel';

window.addEventListener('load', async _ => {
  const codeCanvas = document.querySelector<HTMLCanvasElement>('#codeCanvas');
  const viewfinderVideo = document.querySelector<HTMLVideoElement>('#viewfinderVideo');
  const snapshotCanvas = document.querySelector<HTMLCanvasElement>('#snapshotCanvas');

  const qrChannel = new QrChannel(codeCanvas, viewfinderVideo, snapshotCanvas, 0, 'H', 'Byte');
  const prococolQrChannel = new ProtocolQrChannel(qrChannel, 'TOM', console.log);
  
  // Keep displaying messages forever.
  let counter = 0;
  while (true) {
    counter++;
    // Display the message and wait until we scan a read receipt for it, then display the next.
    await prococolQrChannel.display(counter.toString());
  }
});
