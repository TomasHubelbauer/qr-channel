type QrChannel = { display: (message: string) => void; scan: () => AsyncIterableIterator<string>; };
type System = 'connectionState' | 'iceConnectionState' | 'iceGatheringState' | 'signalingState';

export default class SpikePeer {
  private readonly qrChannel: QrChannel;
  private readonly onLog: (system: System, message: string) => void;

  private readonly peerConnection: RTCPeerConnection;
  private readonly dataChannel: RTCDataChannel;
  private ownOffer: RTCSessionDescriptionInit | undefined;

  public constructor(qrChannel: QrChannel, onLog: (system: System, message: string) => void) {
    this.qrChannel = qrChannel;
    this.onLog = onLog;

    this.peerConnection = new RTCPeerConnection({ iceServers: [ { urls: [ 'stun:stun.l.google.com:19302' ] } ] });

    // TODO: https://github.com/mdn/browser-compat-data/issues/1752
    this.onLog('connectionState', this.peerConnection.connectionState);
    this.peerConnection.addEventListener('connectionstatechange', event => {
      this.onLog('connectionState', this.peerConnection.connectionState);
    });

    this.onLog('iceConnectionState', this.peerConnection.iceConnectionState);
    this.peerConnection.addEventListener('iceconnectionstatechange', event => {
      this.onLog('iceConnectionState', this.peerConnection.iceConnectionState);
    });

    this.onLog('iceGatheringState', this.peerConnection.iceGatheringState);
    this.peerConnection.addEventListener('icegatheringstatechange', event => {
      this.onLog('iceGatheringState', this.peerConnection.iceGatheringState);
    });

    this.onLog('signalingState', this.peerConnection.signalingState);
    this.peerConnection.addEventListener('signalingstatechange', event => {
      this.onLog('signalingState', this.peerConnection.signalingState);
    });

    this.peerConnection.addEventListener('icecandidate', event => {
      this.onLog('iceGatheringState', event.candidate ? '(candidate)' : '(null candidate)');
    });

    // TODO: https://github.com/Microsoft/TypeScript/issues/23245
    this.dataChannel = this.peerConnection.createDataChannel('qr');
  }

  public async broadcast() {
    this.ownOffer = await this.peerConnection.createOffer();
    await this.peerConnection.setLocalDescription(this.ownOffer);
    this.qrChannel.display('O' + this.ownOffer.sdp!);
    for await (const message of this.qrChannel.scan()) {
      throw new Error('TODO: Handle QR message.' + message);
    }
  }
}
