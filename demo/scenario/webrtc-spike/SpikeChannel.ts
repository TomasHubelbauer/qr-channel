export default class SpikeChannel {
    private readonly onDisplay: (message: string) => void;
    private readonly onScan: (message: string) => void;
    private scanPromise: Promise<string> | undefined;
    private scanDeffered: ((message: string) => void) | undefined;

    public constructor(onDisplay: (message: string) => void, onScan: (message: string) => void) {
        this.onDisplay = onDisplay;
        this.onScan = onScan;
        this.reset();
    }

    private reset() {
        this.scanPromise = new Promise(resolve => {
            this.scanDeffered = resolve;
        });
    }

    public notice(message: string) {
        if (this.scanDeffered === undefined) {
            throw new Error('Deffered is undefined');
        }

        this.scanDeffered(message);
    }

    public display(message: string) {
        this.onDisplay(message);
    }

    public async *scan() {
        do {
            const message = await this.scanPromise!;
            this.reset();
            this.onScan(message);
            yield message;
        } while (true);
    }
}
