import SpikeChannel from './SpikeChannel';
import SpikePeer from './SpikePeer';

type Event = { id: string; superIds: null | string[]; };
// TODO: resetting local description to answer
// TODO: Data channel events
// TODO: ICE events
// TODO: Pranswer if needed?
// TODO: Other events?
const events: Event[] = [
    { id: 's', superIds: null },

    // Dead-end
    { id: 'noo', superIds: ['s'] },

    // Happy path. These are split into individual actions and methods on the peer so we can simulate various call orders and breakage scenarios.
    { id: 'npo', superIds: ['s'] },
    { id: 'spord', superIds: ['npo'] },
    { id: 'ca', superIds: ['spord'] },
    { id: 'sald', superIds: ['ca'] },
    { id: 'da', superIds: ['sald'] },
    { id: 'han', superIds: ['da'] },
    { id: 'hasprd', superIds: ['han'] },
    { id: 'ec', superIds: ['hasprd'] },

    // Breakage: notices peer offer again
    { id: 'bnpo', superIds: ['npo'] },

    // Breakage: notices a different peer offer
    { id: 'bndpo', superIds: ['npo'] },

    // Breakage: notices own answer
    { id: 'bnoa', superIds: ['da'] },
];

function* flow(superId: string | null = null, ...flowEvents: Event[]): IterableIterator<Event[]> {
    let hasSubitems = false;
    for (const event of events) {
        if ((superId === null && event.superIds === null) || (event.superIds !== null && event.superIds.includes(superId!))) {
            yield* flow(event.id, ...flowEvents, event);
            hasSubitems = true;
        }
    }

    // Prevent incomplete paths from being returned.
    if (!hasSubitems) {
        yield flowEvents;
    }
}

window.addEventListener('load', async () => {
    const paths = [...flow()];
    for (const path of paths) {
        const titleH2 = document.createElement('h2');
        titleH2.textContent = 'Events:';
        document.body.appendChild(titleH2);

        const connectionStateDiv = document.createElement('div');
        connectionStateDiv.textContent = 'Peer connection state:';
        document.body.appendChild(connectionStateDiv);

        const iceConnectionStateDiv = document.createElement('div');
        iceConnectionStateDiv.textContent = 'ICE connection state:';
        document.body.appendChild(iceConnectionStateDiv);

        const iceGatheringStateDiv = document.createElement('div');
        iceGatheringStateDiv.textContent = 'ICE gathering state:';
        document.body.appendChild(iceGatheringStateDiv);

        const signalingStateDiv = document.createElement('div');
        signalingStateDiv.textContent = 'Signaling state:';
        document.body.appendChild(signalingStateDiv);

        const qrChannelDiv = document.createElement('div');
        qrChannelDiv.textContent = 'QR channel:';
        document.body.appendChild(qrChannelDiv);

        let ownOffer: string | undefined;
        const qrChannel = new SpikeChannel(
            message => {
                ownOffer = message;
                qrChannelDiv.textContent += ' >' + message;
            },
            message => {
                qrChannelDiv.textContent += ' <' + message;
            },
        );
        let peer: SpikePeer;

        titleH2.textContent = 'Flows';
        for (const event of path) {
            titleH2.textContent += ' -> ';
            switch (event.id) {
                case 's': {
                    titleH2.textContent += 'Broadcasts';
                    peer = new SpikePeer(qrChannel, (system, message) => {
                        switch (system) {
                            case 'connectionState': connectionStateDiv.textContent += ' -> ' + message; break;
                            case 'iceConnectionState': iceConnectionStateDiv.textContent += ' -> ' + message; break;
                            case 'iceGatheringState': iceGatheringStateDiv.textContent += ' -> ' + message; break;
                            case 'signalingState': signalingStateDiv.textContent += ' -> ' + message; break;
                            default: throw new Error(`Unexpected system ${system}.`);
                        }
                    });

                    // TODO: Figure out what to do here.
                    // We can't await this because in it, the async loop for receiving messages runs.
                    // But fire-and-forget is not good either as we don't get deterministic order anymore.
                    // We could probably split this into two methods, one for offer and one for scan.
                    // But down the line we'll want to generate combinations of different ICE candidate scenarios.
                    // And I am not sure if that will require simulating ICE candidates coming in deterministically, too.
                    // If yes, we can probably let all of this run from the constructor and then pretend we're getting the ICE.
                    // It would work by using a promise based loop like in the spike channel but instead of messages we'd exhaust collected ICE.
                    peer.broadcast();

                    break;
                }

                // Dead-end:
                case 'noo': {
                    titleH2.textContent += 'Notices own offer';
                    qrChannel.notice(ownOffer!);
                    break;
                }

                // Happy path:
                case 'npo': {
                    titleH2.textContent += 'Notices peer offer';
                    qrChannel.notice('peerOffer'); // TODO: Actual peer offer.
                    break;
                }
                case 'spord': {
                    titleH2.textContent += 'Sets peer offer to remote description';
                    await peer!.setPeerOfferToLocalDescription();
                    break;
                }
                case 'ca': {
                    titleH2.textContent += 'Creates an answer to the peer offer';
                    await peer!.createAnswer();
                    break;
                }
                case 'sald': {
                    titleH2.textContent += 'Sets answer to local description';
                    await peer!.setAnswerToLocalDescription();
                    break;
                }
                case 'da': {
                    titleH2.textContent += 'Displays answer';
                    peer!.displayAnswer();
                    break;
                }
                case 'han': {
                    titleH2.textContent += 'Has answer noticed';
                    // TODO: Send fake scan with a read receipt through the QR channel.
                    break;
                }
                case 'hasprd': {
                    titleH2.textContent += 'Has the answer set to peer remote description';
                    // TODO: ?
                    break;
                }
                case 'ec': {
                    titleH2.textContent += 'Establishes connection';
                    // TODO: Check if connection/data channel is established
                    break;
                }

                // Breakage:
                case 'bnpo': {
                    titleH2.textContent += 'Notices peer offer again';
                    // TODO: Actually should't break, it needs to keep noticing for read receipts!
                    break;
                }

                // Breakage:
                case 'bndpo': {
                    titleH2.textContent += 'Notices a different peer offer';
                    // TODO: Should ignore it or potentially start network if this wasn't a 1:1 scenario.
                    break;
                }

                // Breakage:
                case 'bnoa': {
                    titleH2.textContent += 'Notices own answer';
                    // TODO: Ignore.
                    break;
                }

                default: throw new Error(`Unexpected event ID ${event.id}.`);
            }
        }
    }
});
