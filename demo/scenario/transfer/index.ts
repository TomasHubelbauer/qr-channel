import QrChannel from 'qr-channel';

window.addEventListener('load', _ => {
  let contents = '';
  let isFinished = false;
  let timestamp: number | undefined;

  const viewfinderVideo = document.getElementById('viewfinderVideo') as HTMLVideoElement;
  const snapshotCanvas = document.getElementById('snapshotCanvas') as HTMLCanvasElement;
  const codeCanvas = document.getElementById('codeCanvas') as HTMLCanvasElement;

  const transferProgress = document.getElementById('transferProgress') as HTMLProgressElement;
  const progressSpan = document.getElementById('progressSpan') as HTMLProgressElement;
  const fileInput = document.getElementById('fileInput') as HTMLInputElement;
  const sendButton = document.getElementById('sendButton') as HTMLButtonElement;
  const receiveButton = document.getElementById('receiveButton') as HTMLButtonElement;

  function sendAdvance(length: number) {
    const message = contents.slice(0, length);
    contents = contents.slice(length);
    const portion = transferProgress.max - contents.length;
    transferProgress.value = portion;
    if (timestamp !== undefined) {
      const rate = `${(portion / ((window.performance.now() - timestamp) / 1000)).toFixed()} characters/second`;
      progressSpan.textContent = `${((transferProgress.value / transferProgress.max) * 100).toFixed(2)} % (${transferProgress.value} / ${transferProgress.max}) ${rate}`;  
    }

    return message;
  }

  function receiveAdvance(length: number) {
    return isFinished ? 'DONE' : '';
  }

  fileInput.addEventListener('change', _ => {
    if (fileInput.files && fileInput.files.length === 1) {
      receiveButton.disabled = true;
      const fileReader = new FileReader();
      fileReader.addEventListener('load', async (_) => {
        contents = fileReader.result;
        transferProgress.max = contents.length;

        // const qrChannel = new QrChannel('SEND',
        //   error => error.type !== QrChannel.HEADER_ERROR && (console.dir(error), console.trace()),
        //   (name, message, time) => {
        //     // Start data transfer rate computation from the moment a first countermessage arrived.
        //     if (timestamp === undefined) {
        //       timestamp = window.performance.now();
        //     }

        //     if (message === 'DONE') {
        //       qrChannel.stopScanning();
        //       console.log('Finished');
        //     } else {
        //       qrChannel.advance(sendAdvance(QrChannel.PAYLOAD_SIZE));
        //       console.log('Received a confirmation and displayed next chunk');
        //     }
        //   },
        //   viewfinderVideo,
        //   snapshotCanvas,
        //   codeCanvas,
        // );

        // window.qrChannel = qrChannel;
        // qrChannel.startScanning();
        // qrChannel.advance(sendAdvance(QrChannel.PAYLOAD_SIZE));
        console.log('Displayed initial chunk');
      });

      fileReader.readAsText(fileInput.files[0]);
    }
  });

  sendButton.addEventListener('click', _ => {
    fileInput.click();
  });

  receiveButton.addEventListener('click', async (_) => {
    sendButton.disabled = true;

    // const qrChannel = new QrChannel('RECEIVE',
    //   error => error.type !== QrChannel.HEADER_ERROR && (console.dir(error), console.trace()),
    //   (name, message, time) => {
    //     isFinished = message === '';
    //     contents += message;

    //     console.group(`Received a chunk`);
    //     console.log(message);
    //     console.groupEnd();

    //     qrChannel.advance(receiveAdvance(QrChannel.PAYLOAD_SIZE));
    //     console.log(isFinished ? 'Displayed final configuration' : 'Displayed confirmation');
    //     if (isFinished) {
    //       // Displaying the 'DONE' message and stopping.
    //       qrChannel.stopScanning();
    //       console.log('Finished');

    //       const timestamp = (new Date()).toISOString().replace('T', '-').replace(/:/g, '-');
    //       const downloadA = document.createElement('a');
    //       downloadA.download = `${timestamp}.txt`;
    //       downloadA.href = 'data:text/plain;charset=utf8,' + encodeURIComponent(contents);
    //       document.body.appendChild(downloadA);
    //       downloadA.click();
    //       document.body.removeChild(downloadA);      
    //     }
    //   },
    //   viewfinderVideo,
    //   snapshotCanvas,
    //   codeCanvas,
    // );

    // window.qrChannel = qrChannel;
    // qrChannel.startScanning();
  });
});
