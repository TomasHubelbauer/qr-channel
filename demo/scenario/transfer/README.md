# Transfer

Transfers a file chunk by chunk over QR codes.

The sender displays a chunk and scans for the recipients confirmation code.

The recipient updates the confirmation code with each received chunk.

The process is repeated until the file is transfered in full.

## Running

`yarn start`

## Building

`yarn build`
