# WebRTC

## Running

```sh
yarn start
start index.html
```

## Testing

### Mobile phones

Open the demo on two mobile phones, select the front facing camera of both and set them up so they face each other. Done.

### Laptops with internal cameras

Sit the laptops so they are in front of one another one's camera sees the other's screen and vice versa. Done.

## Building

`yarn build`
