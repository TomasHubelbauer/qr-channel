import QrChannel from 'qr-channel';

export default class Peer {
  private readonly qrChannel: QrChannel;
  private readonly onLog: (system: string, message: string) => void;
  private readonly cycles: string[] = [];
  private cycleIndex = 0;
  private messageNumber = 0;

  constructor(qrChannel: QrChannel, onLog: (system: string, message: string) => void) {
    this.qrChannel = qrChannel;
    this.onLog = onLog;
    window.setInterval(this.cycle.bind(this), 300);
  }

  public async start() {
    const peerConnection = new RTCPeerConnection({ iceServers: [ { urls: [ 'stun:stun.l.google.com:19302' ] } ] });

    this.onLog('iceConnectionState', peerConnection.iceConnectionState);
    peerConnection.addEventListener('iceconnectionstatechange', event => {
      this.onLog('iceConnectionState', peerConnection.iceConnectionState);
    });

    this.onLog('iceGatheringState', peerConnection.iceGatheringState);
    peerConnection.addEventListener('icegatheringstatechange', event => {
      this.onLog('iceGatheringState', peerConnection.iceGatheringState);
    });

    this.onLog('signalingState', peerConnection.signalingState);
    peerConnection.addEventListener('signalingstatechange', event => {
      this.onLog('signalingState', peerConnection.signalingState);
    });

    peerConnection.addEventListener('icecandidate', event => {
      this.onLog('iceGatheringState', event.candidate ? '(candidate)' : '(null candidate)');
    });

    // TODO: https://github.com/Microsoft/TypeScript/issues/23245
    const dataChannel = peerConnection.createDataChannel('qr');

    // TODO: Consider introducing a state field and propagating that to the UI, used to check connection flow

    const offer = await peerConnection.createOffer();
    await peerConnection.setLocalDescription(offer);
    this.show('o', ...offer.sdp!.split(/\r\n/g));

    this.scan();
  }

  private show(...chunks: string[]) {
    for (let index = 0; index < chunks.length; index++) {
      const chunk = chunks[index];
      this.cycles.push(`${this.messageNumber}|${index}|${chunks.length}|${chunk}`);
    }

    this.messageNumber++;
  }

  private async scan() {
    // TODO: Construct chunks to a message first
    for await (const message of this.qrChannel.scan()) {
      // Become an answerer upon seeing someone else's offer
      if (message.startsWith('Ov=0')) {
        await peerConnection.setRemoteDescription({ type: 'offer', sdp: message.substring(1) });
        const answer = await peerConnection.createAnswer();
        await peerConnection.setLocalDescription(answer);
        continue;
      }

      // Open the connection upon seeing someone else's answer
      if (message.startsWith('Av=0')) {
        await peerConnection.setRemoteDescription({ type: 'answer', sdp: message.substring(1) });
        // Peer connection should open now (ignoring ICE for now)
        continue;
      }
    }
  }

  private cycle() {
    if (this.cycles.length > 0) {
      this.qrChannel.display(this.cycles[this.cycleIndex % this.cycles.length]);
      this.cycleIndex++;
      this.onLog('qrChannel', this.cycles[this.cycleIndex % this.cycles.length]);
    }
  }
}
