import QrChannel from 'qr-channel';
import Peer from './Peer';
import { h, mount, patch } from 'petit-dom';

const state = {

};

let app = <div />;
window.addEventListener('load', async () => {
  document.body.appendChild(mount(app));
  render();

  const codeCanvas = document.querySelector<HTMLCanvasElement>('#codeCanvas')!;
  const viewfinderVideo = document.querySelector<HTMLVideoElement>('#viewfinderVideo')!;
  const snapshotCanvas = document.querySelector<HTMLCanvasElement>('#snapshotCanvas')!;
  const qrChannel = new QrChannel(codeCanvas, viewfinderVideo, snapshotCanvas, 0, 'H', 'Byte');

  const connectionStateDiv = document.querySelector<HTMLDivElement>('#connectionStateDiv')!;
  const iceConnectionStateDiv = document.querySelector<HTMLDivElement>('#iceConnectionStateDiv')!;
  const iceGatheringStateDiv = document.querySelector<HTMLDivElement>('#iceGatheringStateDiv')!;
  const signalingStateDiv = document.querySelector<HTMLDivElement>('#signalingStateDiv')!;
  const qrChannelDiv = document.querySelector<HTMLDivElement>('#qrChannelDiv')!;
  const peer = new Peer(qrChannel, (system, message) => {
    switch (system) {
      case 'connectionState': connectionStateDiv.textContent += ' -> ' + message; break;
      case 'iceConnectionState': iceConnectionStateDiv.textContent += ' -> ' + message; break;
      case 'iceGatheringState': iceGatheringStateDiv.textContent += ' -> ' + message; break;
      case 'signalingState': signalingStateDiv.textContent += ' -> ' + message; break;
      case 'qrChannel': qrChannelDiv.textContent = message; break;
      default: throw new Error(`Unexpected system ${system}.`);
    }
  });

  render();
  await peer.start();
});

function render() {
  const temp = app;
  app = (
    <div>
      <div>
        <video id="viewfinderVideo"></video>
        <canvas id="snapshotCanvas"></canvas>
      </div>
      <div id="connectionStateDiv">Peer connection state:</div>
      <div id="iceConnectionStateDiv">ICE connection state:</div>
      <div id="iceGatheringStateDiv">ICE gathering state:</div>
      <div id="signalingStateDiv">Signaling state:</div>
      <div id="qrChannelDiv">QR channel:</div>
      <center>
        <canvas id="codeCanvas" height="600" width="600" />
      </center>
    </div>
  );
  patch(app, temp);
}
