# Throughput

The peers are displaying random messages to one another and at the same time confirming reception of them..

Each peer switches the messages as fast as possible upon receiving "read receipt".

This showcases relative performance between different peers rather well.

## Running

`yarn start`

## Building

`yarn build`
