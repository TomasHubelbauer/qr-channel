import QrChannel from 'qr-channel';

if (!window.location.hash) {
  window.location.hash = prompt('Name:') || '';
}

window.addEventListener('load', _ => {
  let sent = 0;
  let received = 0;

  const viewfinderVideo = document.getElementById('viewfinderVideo') as HTMLVideoElement;
  const snapshotCanvas = document.getElementById('snapshotCanvas') as HTMLCanvasElement;
  const codeCanvas = document.getElementById('codeCanvas') as HTMLCanvasElement;

  const messageDiv = document.getElementById('messageDiv') as HTMLDivElement;
  const rateDiv = document.getElementById('rateDiv') as HTMLDivElement;
  const logsDiv = document.getElementById('logsDiv') as HTMLDivElement;

  // const qrChannel = new QrChannel(
  //   window.location.hash.substr(1),
  //   error => {
  //     console.log(error);
  //     log(JSON.stringify(error));
  //   },
  //   (name, message, time) => {
  //     qrChannel.advance(advance(QrChannel.PAYLOAD_SIZE));
  //     received += message.length;
  //     rateDiv.textContent = `Received ${received} characters in ${(performance.now() / 1000).toFixed(1)} seconds ~= ${(received / (performance.now() / 1000)).toFixed(0)} c/s`;
  //     log(`Received ${message} from ${name} in ${time} ms.`);
  //   },
  //   viewfinderVideo,
  //   snapshotCanvas,
  //   codeCanvas,
  // );

  // window.qrChannel = qrChannel;
  // qrChannel.startScanning();
  // qrChannel.advance(advance(QrChannel.PAYLOAD_SIZE));

  function advance(length: number) {
    sent += length;
    return 'T'.repeat(30);
  }

  function log(message: string) {
    const messageDiv = document.createElement('div');
    messageDiv.textContent = message;
    logsDiv.insertAdjacentElement('afterbegin', messageDiv);
    while (logsDiv.lastElementChild && logsDiv.childElementCount > 10) {
      logsDiv.lastElementChild.remove();
    }
  }
});

declare global {
  interface Window {
    qrChannel: QrChannel;
  }

  interface String {
    repeat: (count: number) => string;
  }
}
