import QrChannel from 'qr-channel';

if (!window.location.hash) {
  window.location.hash = prompt('Name:') || '';
}

window.addEventListener('load', async (_) => {
  const viewfinderVideo = document.getElementById('viewfinderVideo') as HTMLVideoElement;
  const snapshotCanvas = document.getElementById('snapshotCanvas') as HTMLCanvasElement;
  const codeCanvas = document.getElementById('codeCanvas') as HTMLCanvasElement;

  //const qrChannel = new QrChannel(window.location.hash.substr(1), handleError, handlePush, viewfinderVideo, snapshotCanvas, codeCanvas);
  //window.qrChannel = qrChannel;

  function handleError(error: Error) {
    // if (error.type === QrChannel.HEADER_ERROR) {
    //   // Ignore this as it happens quite frequently with frames where there's no QR codes.
    //   return;
    // }

    console.log(error);
  }

  const offerPeerConnection = prepareConnectionAndChannel('o');

  async function handlePush(peer: string, message: string) {
    switch (message[0]) {
      case 'o': {
        // Prepare an answer for the captured offer
        const answerPeerConnection = prepareConnectionAndChannel('a');
        const offer = new RTCSessionDescription({ type: 'offer', sdp: message.slice(1) });
        await answerPeerConnection.setRemoteDescription(offer);
        const answer = await answerPeerConnection.createAnswer();
        await answerPeerConnection.setLocalDescription(answer);
        if (answerPeerConnection.localDescription !== null && answerPeerConnection.localDescription.sdp !== null) {
          //qrChannel.advance('a' + answerPeerConnection.localDescription.sdp);
        }

        break;
      }

      case 'a': {
        // Set answer to our offer peer connection
        const answer = new RTCSessionDescription({ type: 'answer', sdp: message.slice(1) });
        offerPeerConnection.setRemoteDescription(answer);
        break;
      }

      default: throw new Error(`Unexpected first letter of message '${message[0]}'.`);
    }
  }

  const offer = await offerPeerConnection.createOffer();
  await offerPeerConnection.setLocalDescription(offer);
  if (offerPeerConnection.localDescription !== null && offerPeerConnection.localDescription.sdp !== null) {
    //qrChannel.advance('o' + offerPeerConnection.localDescription.sdp);
  }

  //qrChannel.startScanning();

  function prepareConnectionAndChannel(prefix: string) {
    const peerConnection = new RTCPeerConnection({ iceServers: [ { urls: [ 'stun:stun.l.google.com:19302' ] } ] });

    peerConnection.addEventListener('connectionstatechange', _ => console.log('connection state', peerConnection.connectionState));
    peerConnection.addEventListener('iceconnectionstatechange', _ => console.log('ice connection state', peerConnection.iceConnectionState));
    peerConnection.addEventListener('icegatheringstatechange', _ => console.log('ice gathering state', peerConnection.iceGatheringState));
    peerConnection.addEventListener('signalingstatechange', _ => console.log('signaling state', peerConnection.signalingState));
  
    peerConnection.addEventListener('icecandidate', ({ candidate }) => {
      if (candidate !== null && peerConnection.localDescription !== null && peerConnection.localDescription.sdp !== null) {
        //qrChannel.advance(prefix + peerConnection.localDescription.sdp);
      }
    });

    peerConnection.addEventListener('datachannel', ({ channel }) => {
      dataChannel.addEventListener('open', () => {
        console.log('data channel open');
        window.dc = dataChannel;
        dataChannel.send('TEST from ' + window.location.hash);
      });

      dataChannel.addEventListener('message', ({ data }) => {
        console.log('data channel message', data);
      });

      dataChannel.addEventListener('error', () => {
        console.log('data channel error');
      });

      dataChannel.addEventListener('close', () => {
        console.log('data channel close');
      });
    });

    const dataChannel = peerConnection.createDataChannel('qr');

    dataChannel.addEventListener('open', () => {
      console.log('data channel open');
      window.dc2 = dataChannel;
      dataChannel.send('TEST2 from ' + window.location.hash);
    });

    dataChannel.addEventListener('message', ({ data }) => {
      console.log('data channel message', data);
    });

    dataChannel.addEventListener('error', () => {
      console.log('data channel error');
    });

    dataChannel.addEventListener('close', () => {
      console.log('data channel close');
    });

    return peerConnection;
  }
});

declare global {
  interface Window {
    qrChannel: QrChannel;
    dc: RTCDataChannel;
    dc2: RTCDataChannel;
  }

  interface RTCPeerConnection {
    createDataChannel: (name: string) => RTCDataChannel;
    connectionState: string;
    addEventListener(name: 'datachannel', handler: (event: { channel: RTCDataChannel }) => void): void;
  }

  interface RTCDataChannel {
    addEventListener(name: 'open', handler: () => void): void;
    addEventListener(name: 'message', handler: (event: { data: string; }) => void): void;
    addEventListener(name: 'error', handler: () => void): void;
    addEventListener(name: 'close', handler: () => void): void;
    send(data: string): void;
  }
}
