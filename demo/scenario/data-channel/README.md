# Data Channel

Both peers start off by displaying JSON serialized WebRTC offer.
As candidates are gathered, the offer code is updated to include them.

The peers also scan for the other peer's offer and update it as it updates on the other end.
For the first time they see it, they prepare an answer and from there on start displaying that.

Once a data channel is open (two can get opened, one for each peer), the peers agree to shut off the QR channel through the data channel.

The QR channel in this case serves as a local signaling channel for WebRTC.
It could be useful as a basic for decentralized applications, which still rely on the Internet infrastructure (for the P2P communication),
but not on a single centralized signaling channel to allow them to exchange SDP and ICE.
It should be noted that a STUN server is still a centralized component in this system and may be needed to traverse some NAT scenarios.

This demo currently ignores `QrChannel.PAYLOAD_SIZE` to keep it simple (albeit making the code harder to scan).
It will be updated to use chunked cyclic display once implemented in the `webrtc` branch.

## Running

`yarn start`

## Building

`yarn build`
