# QR Channel

A set of packages for using QR codes for exchanging data between two devices in line of sight.

## Installing

- [Undirected unordered unchunked QR channel](src/undirected-unordered-unchunked-qr-channel): `yarn add undirected-unordered-unchunked-qr-channel`
- [Undirected ordered unchunked QR channel](src/undirected-ordered-unchunked-qr-channel): `yarn add undirected-ordered-unchunked-qr-channel`

## Running

### Remotely

[QR Channel demos](http://qr-channel.hubelbauer.net/)

### Locally

Check out the [protocol](demo/protocol/README.md) and [scenario](demo/scenario/README.md) demos.

## Deploying

The site with demos lives on Dokku.

```sh
wsl
git push dokku master
```

## Contributing

- Use the VS Code extension *MarkDown To-Do* to see todos sprinkled around in wiki MarkDown files
- Check out the `todo` directory
- [ ] Figure out a solution for gathering to-do items from code files (maybe extend MarkDown To-Do in scope?)
- Start your work in a personal branch: `git checkout -b patch/…`
- Work and regularly rebase on top of `master`: `git pull origin master`
- Fix conflicts and continue with your work until you're happy with it
- I will merge your changes in `master`: `git pull origin patch/…`
- I will then remove your branch in the remote: `git push -d origin patch/…`
