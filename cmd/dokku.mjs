/* Build all the demos and expose them as a site on Dokku */

import shell from 'shelljs';
import serve from 'serve';


// Serve the demos as they are built to make the app responsive from the get-go
serve(shell.pwd(), { port: process.env.PORT || 4000 });

// Build the skeleton of the index page
shell.echo(`
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf8" />
    <title>QR Channel</title>
    <style>
      body { font: icon; }
    </style>
  </head>
  <body>
    <h1>QR Channel</h1>
    <ul>
`).to('index.html');

// Install demos on the server
shell.ls('-d', 'demo/protocol/*/', 'demo/scenario/*/').forEach(cwd => {
  // Put the link to the built demo to the index page
  shell.echo(`      <li><a href="${cwd}/dist">${cwd}</a></li>`).toEnd('index.html');
  shell.exec('yarn', { cwd });
  shell.exec('yarn build', { cwd });
});

// Finish off the index page HTML
shell.echo(`
    </ul>
  </body>
</html>
`).toEnd('index.html');
