# Read Receipts

We offer splitting messages into chunks in the library itself, so that the library users do not have to track this and can make use of a simple API.
The way this is achieved is we accept messages through `broadcast` or `cast` and cyclically display only chunks of those messages with some metadata.
The messages and their chunks are iterated sequentially, skipping the chunks we already know were received by the counterparty.

In order to know when it is safe so stop displaying a certain chunk, we need some sort of a confirmation of reception from the counterparty.
Once we know all chunks were received, we can stop displaying the message altogether and let the sender know the recipient has received it.
That's what read receipt portion of the message header is for.

We a peer A displays a broadcast and peer B notices it, it extends whatever it is displaying currently (broadcast or cast) by read receipt metadata.
From that point on, peer B tells the world it has seen a chunk (of a particular number) or a message (of a particular) number from a particual peer.
This information is ignored by peers it is not directed to, but is processed by the peer it is directed to, the peer A.

Once the peer A gets a wind of this message chunk read receipt, it marks that chunks as transferred (invoking the send progress event handler)
and no longer shows that chunk of the message. That is, unless it is a broadcast message, in which case it continues showing it for other peers.
But if it is a cast message, it skips that chunk reducing the total amount of chunks it needs to go through in order to transfer the whole message.

Peer A also needs to let the peer B know that it has seen its read receipt and it no longer needs to keep showing it.
If we didn't faciliate this, peer B would have to display a read receipt for any received chunk and the amount of metadata to transfer would soon
grow so large, exchanging it would go so slow, backups would be caused on the sender side (sender not knowing it can stop displaying some chunks in time).

In order to resolve that potential problem, peer A starts displaying a read receipt read receipt for the peer B, so that peer B can notice that and stop.
Until now, the peer A was displaying a broadcast with no metadata, so from now on, its same broadcast (its various chunks being cycled through), come with
a metadata header in which read receipts are being cycled and at this time, the read receipt for B's read receipt is the sole read receipt being shown.

B is still displaying chunks of its own broadcast, but also the read receipt for A's chunk it saw, and once processing the next frame from A,
one with the read receipt read receipt bundled in, it knows that not only did A see one of its chunk (with its read receipt), but also its read receipt.
It marks the seen chunk as seen (unless it is a broadcast chunk) and removes the read receipt it was showing.
From now on B shows a new read receipt for the read receipt from A.

- [ ] Figure out how to prevent broadcast message chunks from generating read receipts, we probably need to keep a backlog for those
- [ ] Figure out if this has flaws or can be streamlined, because that's a lot of data
- [ ] Figure out if limiting to one message at a time would have benefits (no parallel WebRTC offer and candidates, but ultimately faster?)

## Alternatives

### No read receipts

We could either send stuff slowly enough to be reasonably sure the recipient will have time to parse it,
but there are no guaranteed the devices won't move and lose sight of each other's QR codes.
We need to support that, otherwise the fragility of the system would make it useless.

### No distinction between messages and chunks

We could reduce the two numbers (`messageNumber` and `messageChunkNumber`) into one to save space when sending,
but getting rid of this distinction would mean that the recipient would have no information about how to stitch messages back together.

This could work if we limited ourselves to one message at a time, which is a fair suggestion, but not something we want as this time.

### Cycling between message numbers with their associated highest chunk number so far

If we only sent the highest chunk number received so far instead of cycling through all of them,
(this being done in an effort to signal to the other peer they only need to send chunks higher than the one received,)
we would run at risk or not seeing some low chunks, because there is no guarantee we will start seeing the chunks from the first one
as they cycle on the screen we observe.

### Cycling between message numbers with their associated lowest chunk number so far

If we only send the lowest chunk number received so far instead of cycling through all of them,
(this being done in an effort to signal to the other peer they don't need to send chunks lower than the one received,)
we would run at risk of causing extreme backups on the sending end, where by missing one or a few of the first chunks,
but then seeing a lot of higher chunks of a long message, we cause the sender to always be sending the high chunks,
even though they are not needed anymore, all until it cycles through all high chunks and gets back to the lowest chunk
we haven't received yet. This would be less of a problem with short messages, but is not a good general solution.

### Cycling between message numbers with their associated received chunk numbers so far

That's what we're doing now.
This creates way more read receipts that the alternatives where there is only one associated chunk number for each message number,
but solves all the shortcomings of those.
We don't have hard numbers on this yet, but we expect to see this redundance pay off by not causing backups in any situation.

### No cycling, concatenating read receipts in the message header

Avoiding the need to cycle through the metadata by bundling them all into the message header with the data is an insufficient solution.
With multiple peers or even just two peers with multiple messages at one time, the amount of metadata would eclipse the amount of data,
causing the QR code to grow very large (and thus complicate scanning) while providing very low throughput, to no throughout,
if the code were just to grow too large because of all the metadata.

This would be the fastest if we only ever sent one message to one peer at any one time, but doesn't scale beyond that.

### Alternating between pure data messages and read receipt metadata messages

Would this be any good? Who knows. Is the difference against the selected solution only in layout and not in speed?
