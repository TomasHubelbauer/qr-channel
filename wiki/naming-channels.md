# Naming Channels

- [ ] Finish this

## Things To Consider

### If names are used to distinguish peers

This can only works if messages are decorated with a metadata header which would contain the name.

If messages do contain sender name, the received can easily tell the message is not their own message in the reflection and who it came from.

If they don't, the peer has to assume any incoming message is from the right counterparty.
I think this limits the group size to two, can't imagine a scenario where group communication could work without names.
They can tell it's not their own message by checking it's content for the messages they are sending.
If they see the content being received is the same as content being sent, they can assume the message is a captured reflection.
This is not foolproof as for example ICE candidates may be the same sent and being received at the same time, they need a prefix like `o`/`a`.

### How read receipts work

Either there are no read receipts and peer start off showcasting a broadcast initial message and when noticing a counterpart message,
switch to the next message of their own.

Or there are read receipts and they are a part of the message header, like peer names can be.
In this case it's also probably important to distinguish whether a single peer receipt is included per header or an array of them.
Arrays could blow up the header size taking space from the actual message size chunk (if chunked) or just growing the QR code too big.
If one per receipt per message is shown, in a group setting, the same message needs to be show multiple times with the various read receipts.
This can be done be either waiting until the peer sees it's current message's read receipt and transitions to the next message (switch RR with it),
or by cycling through codes showing one message multiple times with different read receipts if needed.

### How messages work

Messsages can either be shown until RR is spotted and then transitioned to the next.
Or they can cycle periodically and be removed from the queue once RR is spotted.
They can be intermixed with RR-only messages (generated internally by the channel) if that's how RR work, they can also be in headers as per above.

### Other things probably

This is really hard for me to keep in my head as a whole and as a result I'll probably "finish" this with mistakes in it I won't be aware of.

## The Derived Formula

- [ ] Derive
